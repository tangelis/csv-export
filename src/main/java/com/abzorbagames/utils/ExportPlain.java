package com.abzorbagames.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.cj.jdbc.MysqlDataSource;

public class ExportPlain {

	String dbUsername = "root";
	String dbPassword;
	String dbHostname = "127.0.0.1";
	int dbPort = 3306;
	boolean showheaders = true;

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Please supply arguments: sql-query [username] [password] (sho_headers:true|false) port");
			return;
		}
		String sqlString = args[0];

		ExportPlain main = new ExportPlain();
		if (args.length > 1) {
			main.dbUsername = args[1];
			main.dbPassword = args[2];
		}

		if (args.length > 3) {
			main.showheaders = Boolean.parseBoolean(args[3]);
		}

		if (args.length > 4) {
			main.dbHostname = args[4];
		}

		if (args.length > 5) {
			main.dbPort = Integer.parseInt(args[5]);
		}

		if (sqlString.equals("pipe")) {
			sqlString = "";
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String input;
				while ((input = br.readLine()) != null) {
					sqlString += input + "\n";
				}
			} catch (IOException io) {
				io.printStackTrace();
			}
		}

		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setUser(main.dbUsername);
		dataSource.setPassword(main.dbPassword);
		dataSource.setServerName(main.dbHostname);
		dataSource.setPort(main.dbPort);
		dataSource.setDatabaseName("platform");

		Connection connection;
		try {
			connection = (Connection) dataSource.getConnection();
			connection.setReadOnly(true);
			connection.setTransactionIsolation(1);
			connection.setAutoCommit(false);
			CsvExporter csvExporter = new CsvExporter(connection, main.showheaders);
			csvExporter.exportData(sqlString, true);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
