package com.abzorbagames.utils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.mutable.MutableLong;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class GameTwistRoundsData {

	private Connection connection;
	private int days;
	private boolean showHeaders;
	private PreparedStatement statement;
	final int[] stringTypes = { Types.CHAR, Types.VARCHAR, Types.CHAR, Types.NVARCHAR, Types.NCHAR,
			Types.CLOB, Types.NCLOB,
			Types.VARBINARY };
	final int[] dateTimeTypes = { Types.DATE, Types.TIME, Types.TIMESTAMP };
	final int stringTypesLen = stringTypes.length;

	public GameTwistRoundsData(Connection connection, boolean showheaders, int days) {
		this.connection = connection;
		this.showHeaders = showheaders;
		this.days = days;
	}

	public void getUsersAndRoundsForGameAndDate(int gameId, Date date, MutableLong users,
			MutableLong rounds, int type)
			throws SQLException {
		PreparedStatement statement = null;
		if (gameId == 1) {
			statement = connection
					.prepareStatement("select count(distinct guid) as active_users, sum(hands_played) as rounds"
							+ " from poker_game_session where date(date_end) = ? and type=?");
		} else if (gameId == 2) {
			statement = connection
					.prepareStatement("select count(distinct guid) as active_users, sum(rounds_played) as rounds"
							+ " from roulette_game_session where date(date_end) = ? and type=?");
		} else if (gameId == 8) {
			statement = connection
					.prepareStatement("select count(distinct guid) as active_users, sum(hands_played) as rounds"
							+ " from baccarat_game_session where date(date_end) = ? and type=?");
		} else if (gameId == 9) {
			statement = connection
					.prepareStatement("select count(distinct guid) as active_users, sum(total_games) as rounds"
							+ " from blackjack_game_session where date(date_end) = ? and type=?");
		}
		if (statement == null) {
			return;
		}
		statement.setDate(1, date);
		statement.setInt(2, type);
		ResultSet rs = statement.executeQuery();
		if (rs.next()) {
			users.setValue(rs.getLong(1));
			rounds.setValue(rs.getLong(2));
		}
		safeClose(statement, rs);
	}

	public void exportData() {
		ResultSet rs = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder builder = new StringBuilder();
		try {
			statement = connection
					.prepareStatement(
							"select dt as Date, game_id as Game,"
									+ " total_bets as 'Stake fun', total_winnings as 'Winnings fun', "
									+ " total_bets - total_winnings as 'Hold/Rake fun', type as Type from game_twist_daily_statistics where dt>=? "
									+ " and dt < date(now()) and type in (1,2) and total_bets>0 order by dt, game_id, type",
							ResultSet.TYPE_FORWARD_ONLY,
							ResultSet.CONCUR_READ_ONLY);
			statement.setDate(1, new Date(DateTime.now().withZone(DateTimeZone.UTC).minusDays(days)
					.getMillis()));
			rs = statement.executeQuery();
			ResultSetMetaData metaData = rs.getMetaData();
			if (showHeaders) {
				showHeaders = false;
				for (int i = 1, j = metaData.getColumnCount(); i <= j; i++) {
					print(builder, metaData.getColumnLabel(i), false, false);
				}
				builder.append(",Active Users, Rounds");
				System.out.println(builder.toString());
				builder.setLength(0);
			}
			while (rs.next()) {
				for (int i = 1, j = metaData.getColumnCount(); i <= j; i++) {
					if (metaData.getColumnName(i).equals("game_id")) {
						switch (rs.getInt(i)) {
						case 1:
							print(builder, "Poker", false, false);
							break;
						case 2:
							print(builder, "Roulette", false, false);
							break;
						case 8:
							print(builder, "Baccarat", false, false);
							break;
						case 9:
							print(builder, "Blackjack", false, false);
							break;

						}
						continue;
					} else if (metaData.getColumnName(i).equals("type")) {
						switch (rs.getInt(i)) {
						case 1:
							print(builder, "Normal", false, false);
							break;
						case 2:
							print(builder, "Promo", false, false);
							break;
						}
						continue;
					}
					String value = null;
					if (rs.getObject(i) != null) {
						value = rs.getString(i);
					} else {
						value = "";
					}
					boolean isStringType = isStringType(metaData, i);
					boolean isDateType = (!isStringType) ? isDateType(metaData, i) : false;
					if (isDateType) {
						java.util.Date date = rs.getTimestamp(i);
						value = simpleDateFormat.format(date);
					}
					print(builder, value, isStringType, isDateType);
				}

				MutableLong users = new MutableLong();
				MutableLong rounds = new MutableLong();
				getUsersAndRoundsForGameAndDate(rs.getInt("Game"), rs.getDate("Date"), users,
						rounds, rs.getInt("type"));
				builder.append("," + users.getValue());
				builder.append("," + rounds.getValue());

				System.out.println(builder.toString());
				builder.setLength(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		safeClose(statement, rs);
	}

	/**
	 * @param statement
	 * @param rs
	 */
	public void safeClose(Statement statement, ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		if (statement != null)
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

	public boolean isStringType(ResultSetMetaData metaData, int i) throws SQLException {
		boolean isStringType = false;
		int type = metaData.getColumnType(i);
		for (int t = 0; t < stringTypesLen; t++) {
			if (stringTypes[t] == type) {
				isStringType = true;
				break;
			}
		}
		return isStringType;
	}

	public boolean isDateType(ResultSetMetaData metaData, int i) throws SQLException {
		boolean isStringType = false;
		int type = metaData.getColumnType(i);
		for (int t = 0; t < dateTimeTypes.length; t++) {
			if (dateTimeTypes[t] == type) {
				isStringType = true;
				break;
			}
		}
		return isStringType;
	}

	private void print(StringBuilder builder, String value, boolean quote, boolean date) {
		if (value.length() == 0) {
			quote = false;
		}
		if (builder.length() > 0) {
			builder.append(',');
		}
		builder.append(value);
	}

}
