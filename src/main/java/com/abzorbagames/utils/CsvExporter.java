package com.abzorbagames.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CsvExporter {

	Connection connection;

	final int[] stringTypes = { Types.CHAR, Types.VARCHAR, Types.CHAR, Types.NVARCHAR, Types.NCHAR,
		Types.CLOB, Types.NCLOB, Types.VARBINARY };
	int[] dateTimeTypes = { Types.DATE, Types.TIME, Types.TIMESTAMP };
	final int stringTypesLen = stringTypes.length;
	final int batchSize = 500;
	boolean showHeaders = true;

	public CsvExporter(Connection connection, boolean showheaders) {
		this.connection = connection;
		this.showHeaders = showheaders;
	}

	public void exportData(String sql, boolean noQuote) {
		if (noQuote) {
			dateTimeTypes = new int[] { Types.TIME, Types.TIMESTAMP };
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Statement statement = null;
		ResultSet rs = null;
		StringBuilder builder = new StringBuilder();
		try {
			statement = (Statement) connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
				ResultSet.CONCUR_READ_ONLY);
			statement.setFetchSize(Integer.MIN_VALUE);
			rs = statement.executeQuery(sql);
			ResultSetMetaData metaData = rs.getMetaData();
			if (showHeaders) {
				showHeaders = false;
				for (int i = 1, j = metaData.getColumnCount(); i <= j; i++) {
					print(builder, metaData.getColumnLabel(i), false, false);
				}
				System.out.println(builder.toString());
				builder.setLength(0);
			}
			while (rs.next()) {
				for (int i = 1, j = metaData.getColumnCount(); i <= j; i++) {
					String value = null;
					if (rs.getObject(i) != null) {
						value = rs.getString(i);
					} else {
						value = "";
					}
					boolean isStringType = isStringType(metaData, i);
					boolean isDateType = (!isStringType) ? isDateType(metaData, i) : false;
					if (isDateType) {
						Date date = rs.getTimestamp(i);
						if (date != null) {
							value = simpleDateFormat.format(date);
						}
					}
					print(builder, value, isStringType && !noQuote, isDateType && !noQuote);
				}
				System.out.println(builder.toString());
				builder.setLength(0);
			}

		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {
			safeClose(statement, rs);
		}

	}

	/**
	 * @param statement
	 * @param rs
	 */
	public void safeClose(Statement statement, ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		if (statement != null)
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

	public boolean isStringType(ResultSetMetaData metaData, int i) throws SQLException {
		boolean isStringType = false;
		int type = metaData.getColumnType(i);
		for (int t = 0; t < stringTypesLen; t++) {
			if (stringTypes[t] == type) {
				isStringType = true;
				break;
			}
		}
		return isStringType;
	}

	public boolean isDateType(ResultSetMetaData metaData, int i) throws SQLException {
		boolean isStringType = false;
		int type = metaData.getColumnType(i);
		for (int t = 0; t < dateTimeTypes.length; t++) {
			if (dateTimeTypes[t] == type) {
				isStringType = true;
				break;
			}
		}
		return isStringType;
	}

	private String escapeString(String inputString) {
		return inputString.replaceAll("|", "");
	}

	private void print(StringBuilder builder, String value, boolean quote, boolean date) {
		if (value.length() == 0) {
			quote = false;
		}
		if (quote) {
			value = value.replaceAll("[^\\p{IsLetter}:\"\'@.\\,\\ 0-9+\\-_\\/]+", "?");
			value = '|' + escapeString(value) + '|';
		} else if (date && value.length() > 0) {
			value = '|' + escapeString(value) + '|';
		}
		if (builder.length() > 0) {
			builder.append(',');
		}
		builder.append(value);
	}

}
