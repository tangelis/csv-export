package com.abzorbagames.utils;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.cj.jdbc.MysqlDataSource;

public class ExportGameTwist {

	String dbUsername = "root";
	String dbPassword;
	String dbHostname = "127.0.0.1";
	int dbPort = 3306;
	boolean showheaders = true;

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err
					.println("Please supply arguments: days [username] [password] (sho_headers:true|false) hostname port");
			return;
		}
		int days = Integer.parseInt(args[0]);

		ExportGameTwist main = new ExportGameTwist();
		if (args.length > 1) {
			main.dbUsername = args[1];
			main.dbPassword = args[2];
		}

		if (args.length > 3) {
			main.showheaders = Boolean.parseBoolean(args[3]);
		}

		if (args.length > 4) {
			main.dbHostname = args[4];
		}

		if (args.length > 5) {
			main.dbPort = Integer.parseInt(args[5]);
		}


		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setUser(main.dbUsername);
		dataSource.setPassword(main.dbPassword);
		dataSource.setServerName(main.dbHostname);
		dataSource.setPort(main.dbPort);
		dataSource.setDatabaseName("platform");

		Connection connection;
		try {
			connection = (Connection) dataSource.getConnection();
			connection.setReadOnly(true);
			connection.setTransactionIsolation(1);
			connection.setAutoCommit(false);
			GameTwistRoundsData export = new GameTwistRoundsData(connection, main.showheaders, days);
			export.exportData();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
